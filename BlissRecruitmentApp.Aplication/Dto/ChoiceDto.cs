﻿namespace BlissRecruitmentApp.Application
{
    public class ChoiceDto
    {
        public string Choice { get; set; }

        public int Votes { get; set; }
    }
}
