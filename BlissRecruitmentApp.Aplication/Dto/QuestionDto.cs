﻿using System;
using System.Collections.Generic;

namespace BlissRecruitmentApp.Application
{
    public class QuestionDto
    {
        public Guid Id { get; set; }
        public string Question { get; set; }

        public string ImageUrl { get; set; }

        public string ThumbUrl { get; set; }

        public DateTime PublishedAt { get; set; }

        public virtual List<ChoiceDto> Choices { get; set; }
    }
}
