﻿using AutoMapper;
using BlissRecruitmentApp.Infrastructure;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlissRecruitmentApp.Application
{
    public class QuestionService : IQuestionService
    {
        private readonly IQuestionRepository questionRepository;
        private readonly IMapper _mapper;
        public QuestionService(IQuestionRepository questionRepository, IMapper mapper)
        {
            _mapper = mapper;
            this.questionRepository = questionRepository;
        }

        public async Task<QuestionDto> FindQuestionById(Guid id)
        {
            return _mapper.Map<QuestionDto>(await questionRepository.GetByIdAsync(id));
        }

        public async Task<List<QuestionDto>> ListQuestions(
            string filter,
            int? page = 1,
            int? limit = 10
            )
        {
            if (filter == string.Empty && !page.HasValue && !limit.HasValue)
            {
                return _mapper.Map<List<QuestionDto>>(await questionRepository.GetAllAsync());
            }

            return _mapper.Map<List<QuestionDto>>(await questionRepository.Pagination(filter, page ?? 1, limit ?? 10));

        }
    }
}
