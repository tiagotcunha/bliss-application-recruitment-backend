﻿using BlissRecruitmentApp.Infrastructure;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlissRecruitmentApp.Application
{
    public interface IQuestionService
    {
        Task<QuestionDto> FindQuestionById(Guid id);
        Task<List<QuestionDto>> ListQuestions(string filter, int? page = 1, int? limit = 10);
    }
}
