﻿using AutoMapper;
using BlissRecruitmentApp.Infrastructure;
using System.Collections.Generic;

namespace BlissRecruitmentApp.Application.Mapper
{

    public class ChoicesMapping : IValueConverter<List<string>, ICollection<ChoiceModel>>
    {
        public ICollection<ChoiceModel> Convert(List<string> sourceMember, ResolutionContext context)
        {
            List<ChoiceModel> choiceModels = new List<ChoiceModel>();
            foreach (string item in sourceMember)
            {
                choiceModels.Add(new ChoiceModel()
                {
                    Choice = item,
                    Votes = 0
                });
            }
            return choiceModels;
        }
    }

    public class UpdateChoicesMapping : IValueConverter<List<ChoiceDto>, ICollection<ChoiceModel>>
    {
        public ICollection<ChoiceModel> Convert(List<ChoiceDto> sourceMember, ResolutionContext context)
        {
            List<ChoiceModel> choiceModels = new List<ChoiceModel>();
            foreach (ChoiceDto item in sourceMember)
            {
                choiceModels.Add(new ChoiceModel()
                {
                    Choice = item.Choice,
                    Votes = item.Votes,
                });
            }
            return choiceModels;
        }
    }

    public class QuestionProfiler : Profile
    {
        public QuestionProfiler()
        {
            CreateMap<CreateQuestionCommand, QuestionModel>()
                .ForMember(member => member.Choices, opts =>
                {
                    opts.ConvertUsing(new ChoicesMapping(), "Choices");
                })
                .IgnoreAllPropertiesWithAnInaccessibleSetter()
                .IgnoreAllSourcePropertiesWithAnInaccessibleSetter();


            CreateMap<UpdateQuestionCommand, QuestionModel>()
                .ForMember(member => member.Choices, opts =>
                {
                    opts.ConvertUsing(new UpdateChoicesMapping(), "Choices");
                });

            CreateMap<QuestionModel, QuestionDto>();

            CreateMap<ChoiceModel, ChoiceDto>();

        }


    }


}
