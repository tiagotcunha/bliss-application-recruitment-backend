﻿using BlissRecruitmentApp.Application;
using BlissRecruitmentApp.Application.Extensions;
using FluentValidation;
using System;

namespace BlissRecruitmentApp.Application.Validators
{
    public class DestinationEmailValidator : AbstractValidator<DestinationEmailCommand>
    {
        public DestinationEmailValidator()
        {
            RuleFor(command => command.ContentUrl)
                .NotEmpty()
                .Length(5, 250)
                .WithMessage("Content_Url is required field.");

            RuleFor(command => command.DestionationEmail)
             .EmailAddress()
             .WithMessage("Email is required field.");


        }
    }
}