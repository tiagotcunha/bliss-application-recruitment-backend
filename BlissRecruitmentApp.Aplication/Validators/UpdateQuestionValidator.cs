﻿using BlissRecruitmentApp.Application;
using BlissRecruitmentApp.Application.Extensions;
using BlissRecruitmentApp.Infrastructure;
using FluentValidation;
using System;
namespace BlissRecruitmentApp.Application.Validators
{

    public class UpdateQuestionValidator : AbstractValidator<UpdateQuestionCommand>
    {
        public UpdateQuestionValidator()
        {
            RuleFor(command => command.Question)
                .Length(5, 250)
                .WithMessage("Question is required field.");

            RuleFor(command => command.ImageUrl)
             .Must(Helper.LinkMustBeAUri)
             .WithMessage("ImageUrl is required field.");


            RuleFor(command => command.ThumbUrl)
             .Must(Helper.LinkMustBeAUri)
             .WithMessage("ThumbUrl is required field.");


            RuleForEach(command => command.Choices)
             .NotEmpty()
             .NotNull()
             .SetValidator(new ChoiceValidator())
             .WithMessage("Choices is required field.");
        }

    }

    public class ChoiceValidator : AbstractValidator<ChoiceDto>
    {
        public ChoiceValidator()
        {
            RuleFor(x => x.Choice).NotEmpty().WithMessage("Please ensure you have selected the B object");
            RuleFor(x => x.Votes).NotEmpty().WithMessage("Please ensure you have selected the B object");
        }
    }
}