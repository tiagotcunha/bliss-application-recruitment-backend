﻿using BlissRecruitmentApp.Application.Extensions;
using BlissRecruitmentApp.Infrastructure;
using FluentValidation;

namespace BlissRecruitmentApp.Application.Validators
{
    public class CreateQuestionValidator : AbstractValidator<CreateQuestionCommand>
    {
        public CreateQuestionValidator(IQuestionRepository questionRepository)
        {
            RuleFor(command => command.Question)
                .NotEmpty()
                .NotNull()
                .Length(5, 250)
                .WithMessage("Question is required field.");

            RuleFor(command => command.Question)
                .MustAsync(async (question, cancellation) =>
                {
                    bool validation = await questionRepository.QuestionAlreadyExist(question.ToLower());
                    return validation;
                })
               .WithMessage("Question already exist.");


            RuleFor(command => command.ImageUrl)
             .NotEmpty()
             .NotNull()
             .Must(Helper.LinkMustBeAUri)
             .WithMessage("ImageUrl is required field.");


            RuleFor(command => command.ThumbUrl)
             .NotEmpty()
             .NotNull()
             .Must(Helper.LinkMustBeAUri)
             .WithMessage("ThumbUrl is required field.");


            RuleForEach(command => command.Choices)
             .NotEmpty()
             .NotNull()
             .WithMessage("Choices is required field.");
        }
    }
}