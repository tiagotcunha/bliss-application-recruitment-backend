﻿using AutoMapper;
using BlissRecruitmentApp.Infrastructure;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
namespace BlissRecruitmentApp.Application
{
    public class QuestionHandler :
    IRequestHandler<CreateQuestionCommand, QuestionModel>,
    IRequestHandler<UpdateQuestionCommand, QuestionModel>
    {

        private readonly IQuestionRepository questionRepository;

        private readonly IMapper _mapper;
        public QuestionHandler(IQuestionRepository questionRepository, IMapper mapper)
        {
            this.questionRepository = questionRepository;
            _mapper = mapper;
        }

        public async Task<QuestionModel> Handle(CreateQuestionCommand request, CancellationToken cancellationToken)
        {
            QuestionModel mappingModel = _mapper.Map<QuestionModel>(request);
            mappingModel.PublishedAt = DateTime.Now;

            object model = await questionRepository.InsertAsync(mappingModel);
            await questionRepository.SaveAsync();

            return model as QuestionModel;
        }

        public async Task<QuestionModel> Handle(UpdateQuestionCommand request, CancellationToken cancellationToken)
        {

            QuestionModel model = await questionRepository.GetByIdAsync(request.Id);

            List<ChoiceModel> choices = _mapper.Map<QuestionModel>(request).Choices as List<ChoiceModel>;

            model.ImageUrl = request.ImageUrl;
            model.PublishedAt = DateTime.Now;
            if (!string.IsNullOrEmpty(request.Question))
            {
                model.Question = request.Question;
            }

            await questionRepository.UpdateChoices(model, choices);

            await questionRepository.SaveAsync();


            return model;
        }
    }
}