﻿using FluentEmail.Core;
using FluentEmail.Core.Models;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
namespace BlissRecruitmentApp.Application
{
    public class ShareHandler :
    IRequestHandler<DestinationEmailCommand, bool>
    {
        private readonly IFluentEmail emailService;
        public ShareHandler(IFluentEmail emailService)
        {
            this.emailService = emailService;
        }

        public async Task<bool> Handle(DestinationEmailCommand request, CancellationToken cancellationToken)
        {

            SendResponse result = await emailService
               .To(request.DestionationEmail)
               .Subject("Subject of email.")
               .Body(request.ContentUrl).SendAsync();

            return result.Successful;
        }
    }
}