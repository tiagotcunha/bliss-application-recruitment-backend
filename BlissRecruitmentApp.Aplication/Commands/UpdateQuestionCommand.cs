﻿using BlissRecruitmentApp.Infrastructure;
using MediatR;
using System;
using System.Collections.Generic;

namespace BlissRecruitmentApp.Application
{
    public class UpdateQuestionCommand : IRequest<QuestionModel>
    {
        public Guid Id { get; set; }

        public string Question { get; set; }

        public string ImageUrl { get; set; }

        public string ThumbUrl { get; set; }

        public List<ChoiceDto> Choices { get; set; }
    }
}
