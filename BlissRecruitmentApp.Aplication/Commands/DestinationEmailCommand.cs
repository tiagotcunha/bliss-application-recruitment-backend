﻿using MediatR;

namespace BlissRecruitmentApp.Application
{
    public class DestinationEmailCommand : IRequest<bool>
    {
        public string DestionationEmail { get; set; }
        public string ContentUrl { get; set; }
    }
}
