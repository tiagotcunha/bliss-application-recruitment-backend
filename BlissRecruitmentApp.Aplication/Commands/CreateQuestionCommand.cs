﻿using BlissRecruitmentApp.Infrastructure;
using MediatR;
using System;
using System.Collections.Generic;

namespace BlissRecruitmentApp.Application
{
    public class CreateQuestionCommand : IRequest<QuestionModel>
    {
        public string Question { get; set; }

        public string ImageUrl { get; set; }

        public string ThumbUrl { get; set; }

        public DateTime PublishedAt { get; set; }
        public List<string> Choices { get; set; }
    }
}
