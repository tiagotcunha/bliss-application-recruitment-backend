# BlissAplication Recruitment

## Setup Project:

Run docker-compose up to setup Postegresql Database. 
```sh
docker-compose up -d
```
If you not have docker installed please setup database manualy and run file sql/schema.sql for creating schema and populate database properly.
```sh
CREATE DATABASE postgres;
```
Next:

Please create following directory.

```sh
C:\email_temp
```
> Note: is required for share endpoint working with expected.

Next:

Open Visual Studio and run project