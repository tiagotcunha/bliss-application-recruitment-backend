

CREATE TABLE IF NOT EXISTS questions (
	id uuid default gen_random_uuid() NOT NULL  PRIMARY KEY,
	question VARCHAR ( 255 ) NOT NULL,
	image_url VARCHAR ( 255 ) NOT NULL,
	thumb_url VARCHAR ( 255 ) NOT NULL,
	published_at TIMESTAMP NOT NULL,
	created_at  TIMESTAMPTZ DEFAULT Now() NOT NULL,
	updated_at TIMESTAMPTZ null
);

CREATE INDEX ON questions ((lower(question)));

CREATE TABLE IF NOT EXISTS choices (
	id uuid default gen_random_uuid() NOT NULL  PRIMARY KEY,
	question_id uuid NOT NULL,
	choice VARCHAR ( 255 ) NOT NULL,
	votes  integer NOT NULL,
	created_at  TIMESTAMPTZ DEFAULT Now() NOT NULL,
	updated_at TIMESTAMPTZ null,
	published_at TIMESTAMP NULL
);







INSERT INTO public.questions (id,question,image_url,thumb_url,published_at,created_at,updated_at) VALUES
	 ('791cf540-fe1c-4c6f-9919-b188f578a918'::uuid,'Question 1','https://via.placeholder.com/150','https://via.placeholder.com/150','2021-05-28 01:06:33.346','2021-05-28 01:06:33.346',NULL),
	 ('df49bd53-80c0-460d-be7a-562d7c203779'::uuid,'Question 2','https://via.placeholder.com/150','https://via.placeholder.com/150','2021-05-28 01:06:33.346','2021-05-28 01:06:33.346',NULL),
	 ('508d9c56-e184-47c3-aadd-0a546c0c4c53'::uuid,'Question 3','https://via.placeholder.com/150','https://via.placeholder.com/150','2021-05-28 01:06:33.346','2021-05-28 01:06:33.346',NULL),
	 ('b73b46f5-b524-485f-88c1-1ad2d15b7388'::uuid,'Question 4','https://via.placeholder.com/150','https://via.placeholder.com/150','2021-05-28 01:06:33.346','2021-05-28 01:06:33.346',NULL),
	 ('0ae08d3c-7127-4100-9b21-738ef2ba617f'::uuid,'Question 5','https://via.placeholder.com/150','https://via.placeholder.com/150','2021-05-28 01:06:33.346','2021-05-28 01:06:33.346',NULL),
	 ('8268d520-df90-4010-b680-d14d05bf2946'::uuid,'Question 6','https://via.placeholder.com/150','https://via.placeholder.com/150','2021-05-28 01:06:33.346','2021-05-28 01:06:33.346',NULL),
	 ('1a2db75f-6185-469b-9945-d3abb99317ee'::uuid,'Question 7','https://via.placeholder.com/150','https://via.placeholder.com/150','2021-05-28 01:06:33.346','2021-05-28 01:06:33.346',NULL),
	 ('1dbd14f2-285a-4b6d-84ec-53b5a79a1a89'::uuid,'Question 8','https://via.placeholder.com/150','https://via.placeholder.com/150','2021-05-28 01:06:33.346','2021-05-28 01:06:33.346',NULL),
	 ('e68fa455-a6fa-4ae0-8b60-39dbf454af8a'::uuid,'Question 9','https://via.placeholder.com/150','https://via.placeholder.com/150','2021-05-28 01:06:33.346','2021-05-28 01:06:33.346',NULL),
	 ('22b59067-c28c-49f7-91e6-fb71546d92bb'::uuid,'Question 10','https://via.placeholder.com/150','https://via.placeholder.com/150','2021-05-28 01:06:33.346','2021-05-28 01:06:33.346',NULL);

INSERT INTO public.questions (id,question,image_url,thumb_url,published_at,created_at,updated_at) VALUES
	 ('9137e5e1-a5d4-4bf2-ad35-a8d6fce8fa10'::uuid,'Favourite programming language?','https://dummyimage.com/600x400/000/fff.png&text=question+1+image+(600x400)','https://dummyimage.com/120x120/000/fff.png&text=question+1+image+(120x120)','2021-05-29 00:51:27.406','2021-05-29 00:51:27.406',NULL);





	 INSERT INTO public.choices (id,question_id,choice,votes,created_at,updated_at,published_at) VALUES
	 ('c1f71c7a-ac11-487a-8891-c9020f695dde'::uuid,'9137e5e1-a5d4-4bf2-ad35-a8d6fce8fa10'::uuid,'Swift',2,'2021-05-29 00:51:27.406','0001-01-01 00:00:00.000','0001-01-01 00:00:00.000'),
	 ('e8c07b93-04c7-46c4-9ad2-fcba76fbcef8'::uuid,'9137e5e1-a5d4-4bf2-ad35-a8d6fce8fa10'::uuid,'Objective-C',0,'2021-05-29 00:51:27.406','0001-01-01 00:00:00.000','0001-01-01 00:00:00.000'),
	 ('2c9ad9ee-b918-47df-a39e-ecce06cd5c7b'::uuid,'9137e5e1-a5d4-4bf2-ad35-a8d6fce8fa10'::uuid,'Ruby',0,'2021-05-29 00:51:27.406','0001-01-01 00:00:00.000','0001-01-01 00:00:00.000'),
	 ('22636476-f726-4dd1-913c-f952100e0a48'::uuid,'9137e5e1-a5d4-4bf2-ad35-a8d6fce8fa10'::uuid,'Python',0,'2021-05-29 00:51:27.406','0001-01-01 00:00:00.000','0001-01-01 00:00:00.000'),
	 ('eb48aefb-3098-4153-ba10-b957fe6600d7'::uuid,'791cf540-fe1c-4c6f-9919-b188f578a918'::uuid,'Elixir',1,'2021-05-28 02:37:06.281',NULL,NULL),
	 ('a7aaac7d-b105-43f0-b6c3-0f8f12732378'::uuid,'791cf540-fe1c-4c6f-9919-b188f578a918'::uuid,'Java',1,'2021-05-28 02:37:06.281',NULL,NULL),
	 ('0c2fdaaf-3f90-488a-a407-844e313d0809'::uuid,'1dbd14f2-285a-4b6d-84ec-53b5a79a1a89'::uuid,'Go Lang',1,'2021-05-28 02:37:06.281',NULL,NULL),
	 ('9725b25c-debe-4ea9-b979-e7a81fd53d72'::uuid,'791cf540-fe1c-4c6f-9919-b188f578a918'::uuid,'C#',1,'2021-05-28 02:37:06.281',NULL,NULL),
	 ('1f47bc22-706e-432e-aa79-33c3d45f2e2d'::uuid,'791cf540-fe1c-4c6f-9919-b188f578a918'::uuid,'C',1,'2021-05-28 02:37:06.281',NULL,NULL),
	 ('724118ba-91f1-4568-9fad-6958adbb27c2'::uuid,'791cf540-fe1c-4c6f-9919-b188f578a918'::uuid,'Javascript',1,'2021-05-28 02:37:06.281',NULL,NULL);