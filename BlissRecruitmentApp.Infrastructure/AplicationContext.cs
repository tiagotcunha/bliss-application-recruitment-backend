﻿using Microsoft.EntityFrameworkCore;

namespace BlissRecruitmentApp.Infrastructure
{
    public class AplicationContext : DbContext
    {
        public AplicationContext(DbContextOptions<AplicationContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<QuestionModel>(entity =>
            {
                entity.Property(entity => entity.CreatedAt)
                .HasDefaultValueSql("now()");

                entity.HasMany(entity => entity.Choices)
                .WithOne(entity => entity.Question)
                .HasForeignKey(d => d.QuestionId);

            });

            modelBuilder.Entity<ChoiceModel>(entity =>
            {

                entity.Property(entity => entity.CreatedAt)
                .HasDefaultValueSql("now()");
            });
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder
                .UseNpgsql()
            .UseSnakeCaseNamingConvention();



        public DbSet<ChoiceModel> Choices { get; set; }
        public DbSet<QuestionModel> Questions { get; set; }

    }
}
