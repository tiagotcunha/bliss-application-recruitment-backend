﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlissRecruitmentApp.Infrastructure
{
    public abstract class GenericRepository<T> : IRepository<T> where T : Entity
    {
        protected AplicationContext context = null;
        protected DbSet<T> table = null;

        public GenericRepository(AplicationContext _context)
        {
            context = _context;
            table = _context.Set<T>();
        }
        public async Task<List<T>> GetAllAsync()
        {
            return await table.ToListAsync();
        }
        public async Task<object> InsertAsync(T obj)
        {
            Microsoft.EntityFrameworkCore.ChangeTracking.EntityEntry<T> model = await table.AddAsync(obj);

            return model;
        }

        public async Task SaveAsync()
        {
            await context.SaveChangesAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            T entity = await table.AsNoTracking().FirstOrDefaultAsync(e => e.Id == id);
            return entity;
        }

        public async Task DeleteAsync(Guid id)
        {
            T existing = await table.FindAsync(id);
            table.Remove(existing);
        }

        public Task<object> Update(T obj)
        {
            throw new NotImplementedException();
        }
    }
}
