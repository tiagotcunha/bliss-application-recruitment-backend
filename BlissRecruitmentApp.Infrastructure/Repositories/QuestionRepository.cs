﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlissRecruitmentApp.Infrastructure
{
    public class QuestionRepository : GenericRepository<QuestionModel>, IQuestionRepository
    {
        public QuestionRepository(AplicationContext aplicationContext) : base(aplicationContext)
        {
        }


        public Task<QuestionModel> UpdateChoices(QuestionModel model, List<ChoiceModel> choicesModel)
        {
            foreach (ChoiceModel choiceModel in choicesModel)
            {

                ChoiceModel existingChild = (model.Choices)
                    .Where(entity => entity.Choice == choiceModel.Choice)
                    .SingleOrDefault();

                if (existingChild != null)
                {
                    existingChild.Votes = existingChild.Votes + choiceModel.Votes;
                    existingChild.UpdatedAt = DateTime.Now;
                }
            }

            return Task.FromResult(model);
        }

        public new async Task<QuestionModel> GetByIdAsync(Guid id)
        {
            QuestionModel entity = await table
                .Include(entity => entity.Choices)
                .FirstOrDefaultAsync(entity => entity.Id == id);

            return entity;
        }

        public new async Task<List<QuestionModel>> GetAllAsync()
        {
            try
            {
                List<QuestionModel> list = await table.Include(entity => entity.Choices)
                    .OrderBy(entity => entity.CreatedAt)
                    .ToListAsync();
                return list;
            }
            catch (System.Exception e)
            {

                throw e;
            }
        }

        public async Task<List<QuestionModel>> Pagination(string filter, int page, int pageSize)
        {
            try
            {
                var skip = (page - 1) * pageSize;

                IQueryable<QuestionModel> query = table
                    .Include(entity => entity.Choices)
                    .OrderByDescending(entity => entity.CreatedAt);

                if (!string.IsNullOrEmpty(filter))
                {
                    query = query.Where(entity => entity.Question.ToLower().Contains(filter) || entity.Choices.Any(choiceEntity => choiceEntity.Choice.ToLower().Contains(filter)));
                }

                List<QuestionModel> list = await query
                    .Skip(skip)
                    .Take(pageSize)
                    .ToListAsync();

                return list;
            }
            catch (System.Exception e)
            {

                throw e;
            }
        }

        public async Task<bool> QuestionAlreadyExist(string question)
        {
            QuestionModel entity = await table
               .Include(entity => entity.Choices)
               .FirstOrDefaultAsync(entity => entity.Question.ToLower() == question);

            return entity == null;
        }
    }
}
