﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlissRecruitmentApp.Infrastructure
{
    public class ChoiceRepository : GenericRepository<ChoiceModel>, IChoiceRepository
    {
        public ChoiceRepository(AplicationContext aplicationContext) : base(aplicationContext)
        {
        }

        public async Task AddRangeAsync(List<ChoiceModel> listOfChoices)
        {
            await table.AddRangeAsync(listOfChoices);
        }
    }
}
