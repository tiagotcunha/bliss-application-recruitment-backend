﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlissRecruitmentApp.Infrastructure
{
    public interface IQuestionRepository : IRepository<QuestionModel>
    {
        Task<QuestionModel> UpdateChoices(QuestionModel model, List<ChoiceModel> choicesModel);
        Task<List<QuestionModel>> Pagination(string filter,int page, int pageSize);
        Task<bool> QuestionAlreadyExist(string name);

    }
}
