﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlissRecruitmentApp.Infrastructure
{
    public interface IRepository<T> where T : class
    {
        Task<object> Update(T obj);
        Task SaveAsync();
        Task<object> InsertAsync(T obj);
        Task<List<T>> GetAllAsync();
        Task DeleteAsync(Guid id);
        Task<T> GetByIdAsync(Guid id);
    }
}
