﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlissRecruitmentApp.Infrastructure
{
    public interface IChoiceRepository : IRepository<ChoiceModel>
    {
        Task AddRangeAsync(List<ChoiceModel> listOfChoices);
    }
}
