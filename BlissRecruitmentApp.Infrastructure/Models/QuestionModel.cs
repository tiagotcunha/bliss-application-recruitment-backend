﻿using System;
using System.Collections.Generic;

namespace BlissRecruitmentApp.Infrastructure
{
    public class QuestionModel : Entity
    {

        public string Question { get; set; }

        public string ImageUrl { get; set; }

        public string ThumbUrl { get; set; }

        public DateTime PublishedAt { get; set; }

        public virtual ICollection<ChoiceModel> Choices { get; set; }
    }
}
