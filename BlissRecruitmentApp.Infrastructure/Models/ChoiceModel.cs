﻿using System;

namespace BlissRecruitmentApp.Infrastructure
{
    public class ChoiceModel : Entity
    {
        public Guid QuestionId { get; set; }

        public string Choice { get; set; }

        public int Votes { get; set; }

        public DateTime PublishedAt { get; set; }

        public QuestionModel Question { get; set; }
    }
}
