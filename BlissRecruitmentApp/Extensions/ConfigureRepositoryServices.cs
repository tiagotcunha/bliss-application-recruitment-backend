﻿using BlissRecruitmentApp.Application;
using BlissRecruitmentApp.Application.Validators;
using BlissRecruitmentApp.Infrastructure;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace BlissRecruitmentApp
{
    public static class ConfigureRepositoryServices
    {
        public static IServiceCollection AddDBConnection(this IServiceCollection services, IConfiguration configuration)
        {
            string connectionString = configuration.GetConnectionString("AplicationContext");
            services.AddDbContext<AplicationContext>(options => options.UseNpgsql(connectionString));

            return services;
        }

        public static IServiceCollection RegisterCommands(this IServiceCollection services)
        {
            services.AddMediatR(typeof(CreateQuestionCommand).GetTypeInfo().Assembly);
            services.AddMediatR(typeof(UpdateQuestionCommand).GetTypeInfo().Assembly);
            services.AddMediatR(typeof(DestinationEmailCommand).GetTypeInfo().Assembly);

            return services;
        }

        public static IServiceCollection AddValidators(this IServiceCollection services)
        {
            services.AddTransient<IValidator<CreateQuestionCommand>, CreateQuestionValidator>();
            services.AddTransient<IValidator<UpdateQuestionCommand>, UpdateQuestionValidator>();
            services.AddTransient<IValidator<DestinationEmailCommand>, DestinationEmailValidator>();
            return services;
        }
    }
}
