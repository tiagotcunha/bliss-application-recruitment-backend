﻿using BlissRecruitmentApp.Application;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace BlissRecruitmentApp.Controllers
{
    [ApiController]
    [Route("share")]
    public class ShareController : ControllerBase
    {
        private readonly ILogger<ShareController> _logger;
        private readonly IMediator _mediator;

        public ShareController(ILogger<ShareController> logger, IMediator mediator)
        {
            _mediator = mediator;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult> ShareAsync(DestinationEmailCommand destinationEmailCommand)
        {
            bool sendEmailResult = await _mediator.Send(destinationEmailCommand);

            if (!sendEmailResult)
            {
                return StatusCode(500);
            }

            var status = new { status = "OK" };

            return Ok(status);
        }
    }
}
