﻿using BlissRecruitmentApp.Application;
using BlissRecruitmentApp.Infrastructure;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlissRecruitmentApp.Controllers
{

    [ApiController]
    [Route("questions")]
    public class QuestionsController : ControllerBase
    {
        private readonly ILogger<QuestionsController> _logger;
        private readonly IMediator _mediator;
        private readonly IQuestionService _questionService;

        public QuestionsController(
            ILogger<QuestionsController> logger,
            IMediator mediator,
            IQuestionService questionService
            )
        {
            _logger = logger;
            _mediator = mediator;
            _questionService = questionService;
        }

        [HttpGet]
        public async Task<IActionResult> Get(
            [FromQuery] int? page,
            [FromQuery] int? limit,
            [FromQuery] string filter
            )
        {
            if (page < 1 || limit < 1)
            {
                return BadRequest("invalid arguments");
            }

            List<QuestionDto> data = await _questionService.ListQuestions(filter, page, limit);

            if (data == null || data.Count == 0)
            {
                return NotFound();
            }

            return Ok(data);
        }

        [HttpPost]
        public async Task<IActionResult> CreateQuestionAsync(CreateQuestionCommand createQuestionCommand)
        {
            QuestionModel request = await _mediator.Send(createQuestionCommand);
            return Ok(request);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateQuestion(string id, [FromBody] UpdateQuestionCommand updateQuestionCommand)
        {
            updateQuestionCommand.Id = Guid.Parse(id);
            QuestionModel command = await _mediator.Send(updateQuestionCommand);
            return Ok(command);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetQuestion(string id)
        {
            QuestionDto data = await _questionService.FindQuestionById(Guid.Parse(id));

            if (data == null)
            {
                return NotFound();
            }

            return Ok(data);
        }
    }
}
